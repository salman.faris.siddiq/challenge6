var express = require("express");
var router = express.Router();
const models = require("../models");
const user_game_biodata = models.user_game_biodata;
/* GET users listing. */
router.get("/user", async function (req, res, next) {
  const getAll = await user_game_biodata.findAll({
    include: "user",
  });
  res.send(getAll);
});

router.get("/", async (req, res) => {
  const getAll = await user_game_biodata.findAll();
  res.send(getAll);
});

router.get("/:id", async (req, res) => {
  const idGame = parseInt(req.params.id);
  const getDetail = await user_game_biodata.findByPk(idGame);
  if (!getDetail) {
    res.send(`data dengan id ${idGame} tidak tersedia`);
  }
  res.status(200).send(getDetail);
});

router.post("/", async function (req, res) {
  const { user_game_id, fullname, age, address } = req.body;
  try {
    await user_game_biodata
      .create({
        user_game_id: user_game_id,
        fullname: fullname,
        age: age,
        address: address,
      })
      .then(function (user_game_biodata) {
        res.status(200).send({
          message: "succesed",
          data: user_game_biodata,
        });
      });
  } catch (error) {
    res.status(500).send({
      error: error.parent.sqlMessage,
    });
  }
});

router.delete("/:id", async (req, res) => {
  const idGame = parseInt(req.params.id);
  try {
    const getDetail = await user_game_biodata.findByPk(idGame);
    if (!getDetail) {
      res.send(`data dengan id ${idGame} tidak tersedia`);
    }
    await user_game_biodata
      .destroy({
        where: {
          user_game_id: idGame,
        },
      })
      .then(function () {
        res.send(`data dengan id ${idGame} sudah terhapus`);
      });
  } catch (error) {
    res.status(500).json({
      error: error.parent.sqlMessage,
    });
  }
});

router.put("/:id", async (req, res) => {
  const idGame = parseInt (req.params.id);
  try {
    const checkId = await user_game_biodata.findByPk(idGame);
    if (!checkId) {
      res.send(`data dengan id ${idGame} tidak tersedia`);
    }
    const { user_game_id,fullname, age, address } = req.body;
    await user_game_biodata
      .update(
        {
          user_game_id: user_game_id,
          fullname: fullname,
          age: age,
          address: address,
        },
        {
          where: {
            id: idGame,
          },
        }
      )
      .then(function () {
        res.json({
          message: `update id ${idGame} succeed`,
          data: {
            fullname,
            age,
            address,
          },
        });
      });
  } catch (error) {
    res.status(500).send({
      error: error,
    });
  }
});

module.exports = router;
