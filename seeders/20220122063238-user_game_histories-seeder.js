'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert("user_game_histories",[
     {
       user_game_id:1,
       time:"05-05-20",
       score:90
     },
     {
      user_game_id:2,
      time:"06-07-20",
      score:95
    },
    {
      user_game_id:3,
      time:"07-08-20",
      score:100
    }
   ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_game_histories",null,{})
  }
};
