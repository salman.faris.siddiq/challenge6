var express = require("express");
var router = express.Router();
const models = require("../models");
const user_game_history = models.user_game_history;

/* GET users listing. */

router.get("/", async function (req, res, next) {
  const getAll = await user_game_history.findAll();
  res.send(getAll);
});

router.get("/user", async function (req, res, next) {
  const getAll = await user_game_history.findAll({
    include: "user",
  });
  res.send(getAll);
});

router.get("/:id", async(req,res)=>{
    const idGame = parseInt (req.params.id)
    const getDetail = await user_game_history.findByPk(idGame)
    if(!getDetail){
        res.status(500).send(`data dengan id ${idGame} tidak tersedia`)
    }
    res.status(200).json({
        message:"succesed",
        data:getDetail
    })

})

router.post("/", async (req, res) => {
  const user_game_id = req.body.user_game_id
  const time = req.body.time
  const score = req.body.score
  try {
    await user_game_history
      .create({
        user_game_id : user_game_id,
        time: time,
        score: score,
      })
      .then(function (user_game_history) {
        res.send(user_game_history);
      });
  } catch (error) {
      res.status(500).send({
          error:error.parent.sqlMessage
      })
  }
});

router.delete("/:id", async(req,res)=>{
    const idGame = parseInt (req.params.id)
    try {
        const getDetail = await user_game_history.findByPk(idGame)
        if(!getDetail){
            res.status(500).send(`data dengan id ${idGame} tidak tersedia`)
        }
        await user_game_history.destroy({
            where:{
                id:idGame
            }
        }).then(function(){
            res.send("suceed deleted")
        })
    } catch (error) {
        res.status(500).send({
            error : error.parent.sqlMessage
        })
    }
  

})

router.put("/:id", async (req,res)=>{
    const idGame = parseInt (req.params.id)
    try {
        const getDetail = await user_game_history.findByPk(idGame)
        if(!getDetail){
            res.status(500).send(`data dengan id ${idGame} tidak tersedia`)
        }
        const {time,score} = req.body
        await user_game_history.update({
            time:time,
            score:score
        },
        {
            where:{
                id:idGame
            }
        }
        ).then(function(){
            res.json({
                message:`data dengan id ${idGame} telah di update`,
                data:{
                    time,
                    score
                }
            })
        })    
    } catch (error) {
        res.status(500).send({
            error: error.parent.sqlMessage
        })
    }
    

})



module.exports = router;
