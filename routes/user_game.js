var express = require("express");
const app = require("../app");
const db = require("../models");
var router = express.Router();
const models = require("../models");
const user_game = models.user_game;
const user_game_biodata = models.user_game_biodata
/* GET users listing. */

router.get("/", async function (req, res, next) {
  const getAll = await user_game.findAll();
  res.send(getAll);
});

router.get("/biodata", async function (req, res, next) {
  const getAll = await user_game.findAll({
    include: "biodata",
  });
  res.send(getAll);
});

router.get("/history", async function (req, res, next) {
  const getAll = await user_game.findAll({
    include: "history",
  });
  res.send(getAll);
});

router.get("/:id", async function (req, res, next) {
  const idUserGame = parseInt(req.params.id);
  const getDetail = await user_game.findByPk(idUserGame);
  res.status(200).json({
    message: `succes get id ${idUserGame}`,
    data: getDetail,
  });
});

router.post("/", async (req, res) => {
  const { username, password } = req.body;
  try {
    await user_game
      .create(
        {
          username: username,
          password: password,
        }
      )
      .then(function (user_game) {
        res.send(user_game);
      });
  } catch (error) {
    res.status(500).json({
      error: error,
    });
  }
});

router.post("/userbiodata",async (req,res)=>{
  const {username,password,fullname,age,address} = req.body
  const saveidGame = await user_game.create({
    username : username,
    password:password
  })
  const userGameId = saveidGame.id
  const biodataGame = await user_game_biodata.create({
    user_game_id : userGameId,
    fullname,
    age,
    address
  })
  const newUserGame = await user_game_biodata.findByPk(biodataGame.id,{
    include:["user"]
  })
  res.json(newUserGame)
})

router.delete("/:id", async (req, res) => {
  const idUserGame = parseInt(req.params.id);
  try {
    const getDetail = await user_game.findByPk(idUserGame);
    if (!getDetail) {
      res.send(`data dengan id ${idUserGame} tidak tersedia`);
    }
    await user_game
      .destroy({
        where: {
          id: idUserGame,
        },
      })
      .then(function () {
        res.send(`data dengan id ${idUserGame} telah terhapus`);
      });
  } catch (error) {
    res.status(500).json({
      error: error.parent.sqlMessage,
    });
  }
});

router.put("/:id", async (req, res) => {
  const idUserGame = parseInt(req.params.id);
  try {
    const getDetail = await user_game.findByPk(idUserGame);
    if (!getDetail) {
      res.send(`data dengan id ${idUserGame} tidak tersedia`);
    }
    const { username, password } = req.body;
    await user_game
      .update(
        {
          username: username,
          password: password,
        },
        {
          where: {
            id: idUserGame,
          },
        }
      )
      .then(function () {
        res.status(200).json({
          message: `data id ${idUserGame} telah terupdate`,
          data: {
            username,
            password,
          },
        });
      });
  } catch (error) {
    res.status(500).json({
      error: error.parent.sqlMessage,
    });
  }
});

module.exports = router;
