'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert("user_game_biodata",[
     {
       user_game_id:1,
       fullname:"Salman Faris Siddiq",
       age:23,
       address:"ciamis"
     },
     {
      user_game_id:2,
      fullname:"Muhammad Zaky",
      age:30,
      address:"aceh"
    },
    {
      user_game_id:3,
      fullname:"Chalid Aulia",
      age:25,
      address:"jakarta"
    }
   ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_game_biodata",null,{})
  }
};
