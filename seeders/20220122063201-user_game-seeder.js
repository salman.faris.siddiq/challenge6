'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert("user_games",[
     {
       username:"HackerRank17",
       password:"12345ABCD"
     },
     {
      username:"Codewars55",
      password:"ABCD12345"
    },
    {
      username:"LeeteCode66",
      password:"password123"
    }
   ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_games",null,{})
  }
};
