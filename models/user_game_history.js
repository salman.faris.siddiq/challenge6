'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_history.belongsTo(models.user_game,{foreignKey:"user_game_id", as:"user"})
    }
  }
  user_game_history.init({
    user_game_id: DataTypes.INTEGER,
    time: DataTypes.DATEONLY,
    score: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_game_history',
    timestamps:false
  });
  return user_game_history;
};